#include <iostream>
#include <stdlib.h>

std::string usage()
{
    std::string output;
    output += "Usage: gcf <unsinged int> <unsigned int>\n\n";
    output += "This program takes two unsigned integers as arguments.\n";
    output += "It then procedes to find the greatest common factor.\n";
    output += "Please pass only unsigend integers.\n";
    output += "Thank you and have a nice day.\n\n";
    return output;
}

bool perfectlyDivisible(unsigned int dividend, unsigned int divisor)
{
    if (dividend % divisor == 0) return true;
    else return false;
}

int main(int argc, char *argv[])
{
    if (argc != 3)  {std::cout << usage();}
    else
    {
        int first = atoi(argv[1]);
        int second = atoi(argv[2]);
        unsigned int gcf = 1;

        if (first < 1 || second < 1) std::cout << usage();
        else
        {
            for (int i = 2; i <= first && i <= second; i++)
            {
                if (perfectlyDivisible(first,i) &&
                    perfectlyDivisible(second,i)  )
                {
                    gcf = i;
                }
            }

            std::cout << "The absolute greatest common factor ever is " << gcf << std::endl;
        }
    }
}
